<?php

namespace Admin\Session;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class SessionPlugin extends AbstractPlugin
{
    protected $sessionService;

    public function setSessionService($sessionService){
        $this->sessionService = $sessionService;
    }

    public function getSessionService(){
        return $this->sessionService;
    }

    public function __invoke() {
        return $this->sessionService;
    }
}
