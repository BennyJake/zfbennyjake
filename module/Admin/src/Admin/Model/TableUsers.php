<?php
/**
 * ZfTable ( Module for Zend Framework 2)
 *
 * @copyright Copyright (c) 2013 Piotr Duda dudapiotrek@gmail.com
 * @license   MIT License 
 */


namespace Admin\Model;

use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Stdlib\ArrayObject;

class TableUsers extends AbstractTableGateway {

    protected $table = 'user';
    
    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Admin\Entity\User(NULL,ArrayObject::ARRAY_AS_PROPS));
        
        $this->initialize();
    }

    public function select($where = null){

        $result = $this->select($where);

        $this->resultSetPrototype->initialize($result);

        return \Zend\Stdlib\ArrayUtils::iteratorToArray($this->resultSetPrototype,false);
    }
}
