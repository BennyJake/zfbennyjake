<?php
/**
 * Copyright (c) 2012 Jurian Sluiman.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @package     ZfcAdmin
 * @subpackage  Controller
 * @author      Jurian Sluiman <jurian@soflomo.com>
 * @copyright   2012 Jurian Sluiman.
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://zf-commons.github.com
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Form\LoginForm;
use Application\Entity\User as EntityUser;
/**
 * Placeholder controller
 *
 * This controller is just here in case you have not defined a controller
 * behind the 'admin' route yourself. If you haven't, you would otherwise
 * get a 404: Page not found error.
 *
 * If you want to override this controller (and action), create a module and
 * put this in the module configuration:
 *
 * <code>
 * <?php
 * return array(
 *     'router' => array(
 *         'routes' => array(
 *             'admin' => array(
 *                 'options' => array(
 *                     'defaults' => array(
 *                         'controller' => 'MyFoo\Controller\OtherController',
 *                         'action'     => 'custom',
 *                     ),
 *                 ),
 *             ),
 *         ),
 *     ),
 * );
 * </code>
 *
 * @package    ZfcAdmin
 * @subpackage Controller
 */
class DashboardController extends AbstractActionController
{
    private $dbAdapter;
    private $session;

    public function indexAction()
    {
        $session = $this->getSession()->get();
        $form = new LogInForm();

        if ($this->getRequest()->isPost()) {
            $user = new EntityUser;
            $form->setInputFilter($user->getInputFilter());
            $form->setData($this->getRequest()->getPost()->toArray());

            if ($form->isValid()) {
                $this->getSession()->set(array('user' => $user->getArrayCopy()));
                $this->loadSession();
                return $this->redirect()->toRoute('dashboard');
            }
        }
    }

    public function dashboardAction(){

    }

    public function getSession()
    {
        if (!$this->session) {
            $this->session = $this->SessionPlugin();
        }
        return $this->session;
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter) {
            $sm = $this->getServiceLocator();
            $this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        }
        return $this->dbAdapter;
    }
}