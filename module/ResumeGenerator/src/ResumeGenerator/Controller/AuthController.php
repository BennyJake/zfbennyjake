<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ResumeGenerator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Helper\ViewModel;

use Application\Model\ResumeTable;

class AuthController extends AbstractActionController
{
    private $dbAdapter;

    public function indexAction()
    {
        session_start();

        $config = $this->getServiceLocator()->get('config');

        $linkedIn=new \HappyR\LinkedIn\LinkedIn($config['linkedin']['appId'],$config['linkedin']['appSecret']);

        if ($linkedIn->isAuthenticated()) {
            //we know that the user is authenticated now. Start query the API

            $user=$linkedIn->api(trim(preg_replace('/\s+/', '', 'v1/people/~:(id,first-name,last-name,email-address,main-address,phone-numbers,
            headline,picture-url,industry,summary,specialties,positions:(id,title,summary,start-date,end-date,
            is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,
            start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,
            publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,
            number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),
            proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,
            start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,
            recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer)')));

            $resumeTable = new ResumeTable($this->getDbAdapter());
            $resumeTable->insert(array('serialized_data' => serialize($this->cleanUserData($user))));

            $response = $this->getResponse();
            $response->setStatusCode(200);

            return $response;

        } elseif ($linkedIn->hasError()) {

            $response = $this->getResponse();
            $response->setStatusCode(500);

            return $response;
        }

        //if not authenticated
        return $this->redirect()->toUrl($linkedIn->getLoginUrl());
    }

    private function cleanUserData($user){

        foreach($user['positions']['values'] as &$position){

            $position['startDate']['fullDate'] = date('F Y',strtotime($position['startDate']['month'].'/01/'.$position['startDate']['year']));


            if(isset($position['isCurrent']) && $position['isCurrent']){

                $position['endDate']['month'] = date('m',strtotime('now'));
                $position['endDate']['year'] = date('Y',strtotime('now'));
            }
            else{
                $position['endDate']['fullDate'] = date('F Y',strtotime($position['endDate']['month'].'/01/'.$position['endDate']['year']));
            }

            //find skills used in these positions, and tally the amount of time each skill was used

            foreach($user['skills']['values'] as &$skills){

                //if we find mention of the skill in the summary, add the number or days at the position to the total
                if(strpos($position['summary'],$skills['skill']['name']) !== FALSE){

                    $startPosition = new \DateTime($position['startDate']['year']."-".$position['startDate']['month']."-01");
                    $endPosition = new \DateTime($position['endDate']['year']."-".$position['endDate']['month']."-01");
                    $interval = $endPosition->diff($startPosition);

                    $skills['time'] += $interval->days;
                }
                else{
                    $skills['time'] += 0;
                }
            }
        }

        $mostSkill = 0;
        foreach($user['skills']['values'] as &$skills){
            if($skills['time'] > $mostSkill){
                $mostSkill = $skills['time'];
            }
        }
        $user['positions']['active'] = $mostSkill;

        return $user;
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter) {
            $sm = $this->getServiceLocator();
            $this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        }
        return $this->dbAdapter;
    }
}
//http://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=app_id&redirect_uri=http%3A%2F%2Fwww.zfbennyjake.dev%2F&state=271a63d4278c8d6d2cb2bb20aac60d93