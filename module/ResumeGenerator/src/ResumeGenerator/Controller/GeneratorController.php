<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ResumeGenerator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DOMPDFModule\View\Model\PdfModel;

//use Application\Model\ResumeTable;

class GeneratorController extends AbstractActionController
{
    private $dbAdapter;

    public function pdfAction()
    {
        $pdf = new PdfModel();
        $pdf->setOption('filename', 'ben-chrisman-resume'); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', '8x11'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"

        $resumeTable = new ResumeTable($this->getDbAdapter());
        $resumeData = $resumeTable->selectMostRecentResume()[0];

        $pdf->setTemplate('layout/pdf');
        $pdf->setVariables(array(
            'info' => $resumeData->getData(),
            )
        );

        return $pdf;
    }

    public function htmlAction()
    {
        $html = new ViewModel();

        $resumeTable = new ResumeTable($this->getDbAdapter());
        $resumeData = $resumeTable->selectMostRecentResume()[0];

        $html->setTemplate('layout/pdf');
        $html->setVariables(array(
                'info' => $resumeData->getData(),
            )
        );

        return $html;
    }

    public function testAction(){

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'ben-chrisman-resume'); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', '8x11'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
        $pdf->setTemplate('layout/test');

        return $pdf;
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter) {
            $sm = $this->getServiceLocator();
            $this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        }
        return $this->dbAdapter;
    }
}