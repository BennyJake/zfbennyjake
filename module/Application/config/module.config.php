<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Servicemanager\AbstractPluginManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Session;
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/]',
                    'defaults' => array(
                        'controller' => 'home',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'auth' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/auth[/]',
                    'defaults' => array(
                        'controller'    => 'Auth',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
            'pdf' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/gen/pdf[/]',
                    'defaults' => array(
                        'controller'    => 'generator',
                        'action'        => 'pdf',
                    ),
                ),
                'may_terminate' => true,
            ),
            'html' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/gen/html[/]',
                    'defaults' => array(
                        'controller'    => 'generator',
                        'action'        => 'html',
                    ),
                ),
                'may_terminate' => true,
            ),
            'zfcadmin' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        'controller' => 'admin',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
            'dashboard' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/admin/dashboard',
                    'defaults' => array(
                        'controller' => 'admin',
                        'action'     => 'dashboard',
                    ),
                ),
                'may_terminate' => true,
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            //http://stackoverflow.com/questions/20456500/zf2-get-global-session-container
            'sessionService' => function(ServiceLocatorInterface $serviceLocator) {
                    $sessionContainer = new \Zend\Session\Container('admin');
                    $sessionService = new Session\SessionService();
                    $sessionService->setSessionContainer($sessionContainer);
                    return $sessionService;
                },
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'controller_plugins' => array(
        'factories' => array(
            'SessionPlugin' => function(AbstractPluginManager $pluginManager){
                    $sessionService = $pluginManager->getServiceLocator()->get('sessionService');
                    $sessionPlugin = new Session\SessionPlugin();
                    $sessionPlugin->setSessionService($sessionService);
                    return $sessionPlugin;
                },
        ),
    ),
    'view_helpers' => array(
        'factories' => array(
            'SessionHelper' => function (AbstractPluginManager $pluginManager){
                    $sessionService = $pluginManager->getServiceLocator()->get('SessionService');
                    $sessionHelper = new Session\SessionHelper();
                    $sessionHelper->setSessionService($sessionService);
                    return $sessionHelper;
                },
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'home' => 'Application\Controller\IndexController',
            'auth' => 'Application\Controller\AuthController',
            'generator' => 'Application\Controller\GeneratorController',
            'admin' => 'Application\Controller\AdminController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/pdf'           => __DIR__ . '/../view/resume/pdf/index.phtml',
            'layout/admin'          => __DIR__ . '/../view/layout/admin.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
